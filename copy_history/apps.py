from django.apps import AppConfig


class CopyHistoryConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'copy_history'
