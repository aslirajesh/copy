from django.db import models


# Create your models here.

class CopyText(models.Model):
    text = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = "Copy Text"
        verbose_name_plural = "Copy Texts"
        ordering = ["-created_at"]

    def __str__(self):
        return self.text
