from drf_yasg import openapi
from rest_framework import viewsets, status
from rest_framework.permissions import AllowAny
from drf_yasg.utils import swagger_auto_schema
from rest_framework.response import Response

from copy_history.models import CopyText
from copy_history.serializers import CopyTextSerializer


class PostTextTestViewSet(viewsets.ViewSet):
    permission_classes = [AllowAny]

    @swagger_auto_schema(
        tags=["Post Text"],
        operation_summary="Post text",
        request_body=openapi.Schema(
            type=openapi.TYPE_OBJECT,
            properties={
                "text": openapi.Schema(
                    type=openapi.TYPE_STRING,
                    description="Text",
                ),
            },
            required=["text"],
        ),
    )
    def create(self, request, *args, **kwargs):
        # get text from request body
        text = request.data.get("copied_text", None)
        if text:
            # add text to Copy Text model
            obj = CopyText.objects.create(text=text)
            obj.save()
            return Response(
                {"message": "Text received"},
                status=status.HTTP_200_OK
            )
        else:
            return Response(
                {"message": "Text not received"},
                status=status.HTTP_400_BAD_REQUEST
            )

    @swagger_auto_schema(
        tags=["Post Text"],
        operation_summary="Get latest 5 text",
    )
    def list(self, request, *args, **kwargs):
        # get 5 latest text from Copy Text model
        queryset = CopyText.objects.all().order_by("-created_at")[:5]
        # serialize the queryset
        queryset = CopyTextSerializer(queryset, many=True).data
        return Response(
            {"data": queryset},
            status=status.HTTP_200_OK
        )

