from rest_framework import serializers

from copy_history.models import CopyText


class CopyTextSerializer(serializers.ModelSerializer):
    class Meta:
        model = CopyText
        fields = ["text"]
